import express from 'express';
import pg from 'pg';

let client = new pg.Client({});
// client.connect();

let app = express();

app.get('/:table', async (req, res, next) => {
	let { fields, joins, ...query } = req.query;

	let table = JSON.stringify(req.params.table);

	/* select */
	let field = `*`;
	if (fields) {
		field = (req.query.fields as string)
			.split(',')
			.map((field) => JSON.stringify(field))
			.join(', ');
	}

	/* where */
	let keys = Object.keys(query);
	let params: string[] = [];
	let where = keys
		.map((key, i) => {
            params.push(req.query[key] as string);
            let field = JSON.stringify(key)
			return `${field} = $${i + 1}`;
		})
		.join(' and ');
	if (where !== '') {
		where = `where ` + where;
	}

	/* join */
	let join = ``;
	if (joins) {
		let refTables = (joins as string).split(',');
		for (let refTable of refTables) {
			let refField = JSON.stringify(refTable + '_id');
			refTable = JSON.stringify(refTable);
			join += `
    inner join ${refTable} on ${refTable}.id = ${table}.${refField}
    `
		}
	}

	let sql = `
    select ${field} from ${table}
    ${join}
    ${where}
    `;

	if ('dev') {
		res.json({ sql, params });
		return;
	}
	try {
		let result = await client.query(sql, params);
		res.json(result.rows);
	} catch (e) {
		res.status(400).json({
			sql,
			params,
			error: e.toString(),
		});
	}
});

app.use(express.static('public'));

let PORT = 8100;
app.listen(8100, () => {
	console.log('listening on http://localhost:' + PORT);
});
